const fs = require('fs');
const del = require('del');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');
const slue = require('slue');
const slueStream = require('slue-stream');
const $ = require('slue-plugins-loader')();
const bs = require('browser-sync').create();
const slueVars = require('./plugins/slueVars');

let env = process.argv.slice(2);

const notifyError = function(err, task, name) {
    $.notify.onError({
        title: task,
        subtitle: 'Failure!',
        message: `${name} error: <%= err.message %>`,
        sound: 'Beep'
    })(err);
    this.end();
};

slue.task('clean', function() {
    del.sync(`./${env}`);
});

slue.task('eslint', function() {
    return slue.read('./src/**/*.js')
        .pipe($.plumber({
            errorHandler(err) {
                notifyError.call(this, err, 'eslint', 'js lint')
            }
        }))
        .pipe($.eslint())
        .pipe($.eslint.format())
        .pipe($.eslint.failAfterError());
});

slue.task('copy', function() {
    return slue.read('./src/assets/**/*.*').pipe(slue.write(`./${env}/assets`));
});

slue.task('less', function() {
    // css后处理器列表
    let processors = [
        // 自动补前缀，根据浏览器列表决定
        autoprefixer(),
        // css优化处理
        cssnano({
            colormin: {
                legacy: true
            },
            core: false,
            zindex: false,
            discardUnused: {
                keyframes: false // 不删除未使用的keyframes声明
            }
        })
    ];
    return slue.read(`./src/**/all.less`)
        .pipe($.plumber({
            errorHandler(err) {
                notifyError.call(this, err, 'less', 'less')
            }
        }))
        .pipe($.less())
        .pipe(slueVars())
        .pipe($.postcss(processors))
        .pipe(slue.write(`./${env}`));
});

slue.task('sprite', function() {
    return slue.read(`./${env}/**/*.css`)
        .pipe($.slueSpritesmith({
            // 源图位置
            imagePath: `./${env}/assets/images/sprite`,
            // 合成后图片放置位置
            spritePath: `./${env}/assets/images/sprite.png`,
            // 处理后的css放置位置
            cssPath: `./${env}/assets/style/all.css`
        }))
});

slue.task('clean-sprite', () => {
    del.sync(`./${env}/assets/images/sprite`);
});

slue.task('md5-assets', () => {
    return slue.read(`./${env}/assets/**/*.{jpg,jpeg,png,gif,woff,eot,ttf}`)
        .pipe($.rev())
        .pipe(slue.write(`./${env}/assets`))
        .pipe($.revDeleteOriginal())
        .pipe($.rev.manifest())
        .pipe(slue.write(`./${env}/rev/assets`))
});

slue.task('rev-assets', () => {
    return slue.read([
            `./build/rev/assets/*.json`,
            `./build/**/*.css`,
            `./build/**/*.js`
        ])
        .pipe($.revCollector({
            replaceReved: true
        }))
        .pipe(slue.write(`./${env}`))
});

slue.task('uglify', () => {
    return slue.read(`./${env}/**/*.js`)
        .pipe($.plumber({
            errorHandler(err) {
                notifyError.call(this, err, 'uglify', 'js uglify')
            }
        }))
        .pipe($.uglify({
            compress: {
                drop_console: true
            },
            output: {
                comments: false,
                keep_quoted_props: true
            }
        }))
        .pipe(slue.write(`./${env}`))
});

slue.task('cssmin', () => {
    return slue.read(`./${env}/**/*.css`)
        .pipe($.plumber({
            errorHandler(err) {
                notifyError.call(this, err, 'cssmin', 'css minify')
            }
        }))
        .pipe($.cleanCss())
        .pipe(slue.write(`./${env}`))
});

slue.task('md5-css', () => {
    return slue.read(`./${env}/**/*.css`)
        .pipe($.rev())
        .pipe(slueVars())
        .pipe(slue.write(`./${env}`))
        //.pipe($.revDeleteOriginal())
        .pipe($.rev.manifest())
        .pipe(slue.write(`./${env}/rev/css`))
});

slue.task('md5-js', () => {
    return slue.read(`./${env}/**/*.js`)
        .pipe($.rev())
        .pipe(slueVars())
        .pipe(slue.write(`./${env}`))
        //.pipe($.revDeleteOriginal())
        .pipe($.rev.manifest())
        .pipe(slue.write(`./${env}/rev/js`))
});

slue.task('rev-js', () => {
    return slue.read([
            `./${env}/rev/js/*.json`,
            `./${env}/index.html`,
            `./${env}/**/*.js`
        ])
        .pipe($.revCollector({
            replaceReved: true
        }))
        .pipe(slue.write(`./${env}`))
});

slue.task('useHtml', () => {
    return slue.injectVars('./src/index.html').pipe(slue.write(`./${env}`));
});

slue.task('watch', () => {
    let watcher = slue.watch('./src/**/*.{less,jpg,jpeg,png,gif,woff,eot,ttf,vue}');
    watcher.on('change', () => {
        $.sequence(['copy', 'less', 'sprite', 'reload'])();
    });
});

slue.on('sluepack.watcher.change', function() {
    $.sequence('reload');
});

slue.task('server', () => {
    bs.init({
        server: `./${env}`
    });
});

slue.task('reload', function() {
    bs.reload();
});

let devTasks = [
    'clean',
    'eslint',
    'copy',
    '__sluepack__',
    'less',
    'sprite',
    'useHtml',
    'watch',
    'server'
];
slue.task('dev', $.sequence(devTasks));

let buildTasks = [
    'clean',
    'eslint',
    'copy',
    '__sluepack__',
    'less',
    'sprite',
    'clean-sprite',
    'md5-assets',
    'rev-assets',
    'uglify',
    'cssmin',
    'md5-css',
    'md5-js',
    'rev-js',
    'useHtml'
];
slue.task('build', $.sequence(buildTasks));