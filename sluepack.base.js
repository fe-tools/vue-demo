var path = require('path');
module.exports = {
    // 要注入的html
    //injectTo: './src/index.html',
    // 入口文件
    entry: {
        app: './src/main.js',
        first: './src/pages/first/first.vue',
        second: './src/pages/second/second.vue'
    },
    exclude: function(opts) {
        if (opts.moduleId.match(/assets/)) {
            return true;
        }

        let regular = /(^crm)|(^base)|(^fs)|(^paas-)|(^vue-selector-input)/;
        return regular.test(opts.moduleId);
    },
    alias: {
        root: path.join(__dirname, './src'),
        components: path.join(__dirname, './src/components'),
        modules: path.join(__dirname, './src/modules')
    },
    // 打包输出目录
    output: {
        // 文件输出的根目录
        root: path.resolve(__dirname, './build'),
        // js文件输出目录(相对root目录)
        jsPath: './js',
        // 除js文件外，其他静态文件输出目录，包括的包出的css、图片等文件、字体文件等(相对root目录)
        staticPath: './static/'
    },
    // 监听文件变化，开启增量编译摸索
    // watch: true,
    // 环境变量，development or production
    mode: 'production'
};