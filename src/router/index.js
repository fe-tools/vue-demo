import Router from 'vue-router';
import firstPage from '../pages/first';
import secondPage from '../pages/second';

export default new Router({
  routes: [{
    path: '/',
    redirect: '/first'
  }, {
    path: '/first',
    name: 'HelloWorld',
    component: firstPage
  }, {
    path: '/second',
    name: 'HelloWorld',
    component: secondPage
  }]
});