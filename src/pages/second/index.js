module.exports = function() {
  return new Promise(function(resolve) {
    require.async('./second', function(component) {
      resolve(component);
    });
  });
};