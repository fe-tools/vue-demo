var path = require('path');

let processArgvs = process.argv.slice(2);
let sluepackConfig = require('./sluepack.base');
if (processArgvs == 'dev') {
    sluepackConfig.watch = true;
    sluepackConfig.sourceMap = true;
    sluepackConfig.mode = 'production';
    sluepackConfig.output.root = path.resolve(__dirname, './dev');
}

module.exports = sluepackConfig;