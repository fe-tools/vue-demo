const path = require('path');
const slueVars = require('slue-vars');

function varFavtory(file) {
    let vars = {};
    let origPath = file.revOrigPath || file.path;
    let basename = path.basename(origPath);
    let extname = path.extname(origPath);
    let key = basename.replace(extname, '');
    let val = file.path.replace(path.resolve(file._cwd, file._base), '')
    vars[key] = val;
    return vars;
}
module.exports = function() {
    return slueVars(varFavtory);
}